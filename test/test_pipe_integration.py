import os

from bitbucket_pipes_toolkit.test import PipeTestCase


class SlackNotifyTestCase(PipeTestCase):

    def test_default_success(self):
        result = self.run_container(environment={
            "WEBHOOK_URL": os.getenv("WEBHOOK_URL"),
            "MESSAGE": "Default test"
        })

        self.assertRegex(result, rf'✔ Notification successful.')

    def test_exit_code_passed_success(self):
        result = self.run_container(environment={
            "BITBUCKET_EXIT_CODE": "0",
            "WEBHOOK_URL": os.getenv("WEBHOOK_URL"),
            "MESSAGE": "exit code passed"
        })

        self.assertRegex(result, rf'✔ Notification successful.')

    def test_exit_code_started_success(self):
        result = self.run_container(environment={
            "BITBUCKET_EXIT_CODE": "started",
            "WEBHOOK_URL": os.getenv("WEBHOOK_URL"),
            "MESSAGE": "exit code started"
        })

        self.assertRegex(result, rf'✔ Notification successful.')

    def test_exit_code_failed_success(self):
        result = self.run_container(environment={
            "BITBUCKET_EXIT_CODE": "-1",
            "WEBHOOK_URL": os.getenv("WEBHOOK_URL"),
            "MESSAGE": "exit code failed"
        })

        self.assertRegex(result, rf'✔ Notification successful.')

    def test_double_quotes_in_message_success(self):
        result = self.run_container(environment={
            "WEBHOOK_URL": os.getenv("WEBHOOK_URL"),
            "MESSAGE": '"Double quotes in message test"'
        })

        self.assertRegex(result, rf'✔ Notification successful.')

    def test_no_message_success(self):
        result = self.run_container(environment={
            "WEBHOOK_URL": os.getenv("WEBHOOK_URL"),
        })

        self.assertRegex(result, rf'✔ Notification successful.')

    def test_empty_message_success(self):
        result = self.run_container(environment={
            "WEBHOOK_URL": os.getenv("WEBHOOK_URL"),
            "MESSAGE": ""
        })

        self.assertRegex(result, rf'✔ Notification successful.')

    def test_debug_flag_success(self):
        result = self.run_container(environment={
            "WEBHOOK_URL": os.getenv("WEBHOOK_URL"),
            "MESSAGE": "Debug flag test!",
            "DEBUG": "true"
        })

        self.assertRegex(result, rf'✔ Notification successful.')

    def test_default_failed(self):
        BAD_WEBHOOK_URL = "https://hooks.slack.com/services/T00000000/B00000000/XXXXXXXXXXXXXXXXXXXXXXXX"

        result = self.run_container(environment={
            "WEBHOOK_URL": BAD_WEBHOOK_URL,
            "MESSAGE": "Bad webhook url"
        })

        self.assertRegex(result, rf'✖ Notification failed.')
