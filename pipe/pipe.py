import requests
from bitbucket_pipes_toolkit import Pipe, yaml, get_variable


# global variables
SLACK_APP_DEFAULT_COLOR = "#439FE0"
SLACK_APP_ERROR_COLOR = "#FF0000"
SLACK_APP_PASSED_COLOR = "#00FF00"
REQUESTS_DEFAULT_TIMEOUT = 10
BASE_SUCCESS_MESSAGE = "Notification successful"
BASE_FAILED_MESSAGE = "Notification failed"


# defines the schema for pipe variables
schema = {
    "WEBHOOK_URL": {
        "type": "string",
        "required": True
    },
    "MESSAGE": {
        "type": "string",
        "required": False,
        "default": ""
    },
    "DEBUG": {
        "type": "boolean",
        "default": False
    }
}


class SlackNotifyPipe(Pipe):
    def run(self):
        super().run()

        webhook_url = self.get_variable("WEBHOOK_URL")
        optional_message = self.get_variable("MESSAGE")
        debug = self.get_variable("DEBUG")

        # get pipelines specific variables
        workspace = get_variable('BITBUCKET_WORKSPACE', default='local')
        repo = get_variable('BITBUCKET_REPO_SLUG', default='local')
        build = get_variable('BITBUCKET_BUILD_NUMBER', default='local')
        exit_code = get_variable('BITBUCKET_EXIT_CODE', default='started')
        branch = get_variable('BITBUCKET_BRANCH', default='no_branch')

        try:
            version_file = open(".version/Version.txt", "r")
            version = version_file.read()
        except FileNotFoundError:
            version = "Can't read version!"
        finally:
            version_file.close()

        if debug:
            self.log_info("Enabling debug mode.")
        self.log_info("Sending notification to Slack...")

        headers = {'Content-Type': 'application/json'}

        message_string = "Message: " + optional_message + "\n"
        repo_string = "Repository: " + repo + "\n"
        branch_string = "Branch: " + branch + "\n"
        version_string = "Version: " + version + "+b." + build + "\n"

        if exit_code == "started":
            status = f"Status: <https://bitbucket.org/{workspace}/{repo}/addon/pipelines/home#!/results/{build}|STARTED>"
            color = SLACK_APP_DEFAULT_COLOR
        elif exit_code == "0":
            status = f"Status: <https://bitbucket.org/{workspace}/{repo}/addon/pipelines/home#!/results/{build}|SUCCESS>"
            color = SLACK_APP_PASSED_COLOR
        else:
            status = f"Status: <https://bitbucket.org/{workspace}/{repo}/addon/pipelines/home#!/results/{build}|FAILED>"
            color = SLACK_APP_ERROR_COLOR

        if(optional_message == ""):
            message = repo_string + branch_string + version_string + status
        else:
            message = message_string + branch_string + repo_string + version_string + status

        payload = {
            "attachments": [
                {
                    "fallback": message,
                    "color": color,
                    "text": message,
                    "mrkdwn_in": ["pretext"]
                }
            ]
        }

        try:
            response = requests.post(
                url=webhook_url,
                headers=headers,
                json=payload,
                timeout=REQUESTS_DEFAULT_TIMEOUT
            )
        except requests.exceptions.Timeout as error:
            error_message = self.create_message(
                BASE_FAILED_MESSAGE,
                'Request to Slack timed out',
                error
            )
            self.fail(error_message)
        except requests.ConnectionError as error:
            error_message = self.create_message(
                BASE_FAILED_MESSAGE,
                'Connection Error',
                error
            )
            self.fail(error_message)

        self.log_info(f"HTTP Response: {response.text}")

        # https://api.slack.com/messaging/webhooks
        if 200 <= response.status_code <= 299:
            self.success(BASE_SUCCESS_MESSAGE)
        else:
            self.fail(BASE_FAILED_MESSAGE)

    def create_message(self, base_message, error_message, error_text):
        message = '{}: {}{}'.format(
            base_message,
            error_message,
            f': {error_text}' if self.get_variable("DEBUG") else f'.'
        )
        return message


if __name__ == '__main__':
    with open('/pipe.yml', 'r') as metadata_file:
        metadata = yaml.safe_load(metadata_file.read())
    pipe = SlackNotifyPipe(schema=schema, pipe_metadata=metadata, check_for_newer_version=True)
    pipe.run()
