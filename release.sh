#!/bin/bash

set -e

source ./setVersion.sh
export set GIT_ASKPASS=$(pwd)/gitAskPass.sh

git clone https://${BITBUCKET_USERNAME}@bitbucket.org/hemrnd/slack-notify-pipe.git
cd slack-notify-pipe
git config user.name "Service User"
git config user.email "serviceuser@hem-e.pl"

cp ../pipe.yml ./pipe.yml
git add .
git commit -m "Updated to version ${VERSION}"
git tag "v${VERSION}"
git push origin
