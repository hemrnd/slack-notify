#!/bin/bash

set -e

source ./setVersion.sh
docker login -u $DOCKER_USER -p $DOCKER_PASSWORD
docker push hemelectronics/${BITBUCKET_REPO_SLUG}:$VERSION