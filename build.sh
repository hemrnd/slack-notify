#!/bin/bash

set -e

source ./setVersion.sh
./generate-pipe.yml.sh
docker build -t hemelectronics/${BITBUCKET_REPO_SLUG}:$VERSION .