
#!/bin/bash

set -e

source ./setVersion.sh

envsubst '${BITBUCKET_REPO_SLUG} $VERSION' < pipe.yml.template > pipe.yml

