#!/bin/bash

set -e

if ! [[ -v VERSION ]]; then
    export set VERSION=`cat .version/Version.txt`
    echo "Version $VERSION"
fi
